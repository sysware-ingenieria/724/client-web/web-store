import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import 'hammerjs';

/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule } from './app.material';
/*
************************************************
*     principal component
*************************************************
*/
import { AppComponent } from './app.component';
/*
************************************************
*     modules of  your app
*************************************************
*/
 import {  WelcomeModule } from "./welcome/welcome.module";
 import {  AuthenticationModule } from "./authentication/authentication.module";
 import {  DashboardModule } from "./dashboard/dashboard.module";
/*
************************************************
*     routing of  your app
*************************************************
*/
import { AppRouting } from './app.routing';
/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserModule,
    RouterModule.forRoot(AppRouting),
    BrowserAnimationsModule,
    MaterialModule,
    WelcomeModule,
    AuthenticationModule,
    DashboardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
