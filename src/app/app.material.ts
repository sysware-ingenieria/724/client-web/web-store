import { NgModule } from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {
        // Form  Controls
         MdAutocompleteModule,
         MdCheckboxModule,
         MdDatepickerModule,
         MdInputModule,
         MdRadioModule,
         MdSelectModule,
         MdSliderModule,
         MdSlideToggleModule,
        // Navigation
         MdMenuModule,
         MdSidenavModule,
         MdToolbarModule,
        // Layout
         MdListModule,
         MdGridListModule,
         MdCardModule,
         MdTabsModule,
         // Button & Indicators
         MdButtonModule,
         MdButtonToggleModule,
         MdChipsModule,
         MdIconModule,
         MdProgressSpinnerModule,
         MdProgressBarModule,
         // Popups & Modals
         MdDialogModule,
         MdTooltipModule,
         MdSnackBarModule,
         // Data Table
         MdTableModule,
         MdSortModule,
         MdPaginatorModule


       } from '@angular/material';

@NgModule({
  imports: [
          BrowserModule,
          // Form  Controls
          MdAutocompleteModule,
          MdCheckboxModule,
          MdDatepickerModule,
          MdInputModule,
          MdRadioModule,
          MdSelectModule,
          MdSliderModule,
          MdSlideToggleModule,
         // Navigation
          MdMenuModule,
          MdSidenavModule,
          MdToolbarModule,
         // Layout
          MdListModule,
          MdGridListModule,
          MdCardModule,
          MdTabsModule,
          // Button & Indicators
          MdButtonModule,
          MdButtonToggleModule,
          MdChipsModule,
          MdIconModule,
          MdProgressSpinnerModule,
          MdProgressBarModule,
          // Popups & Modals
          MdDialogModule,
          MdTooltipModule,
          MdSnackBarModule,
          // Data Table
          MdTableModule,
          MdSortModule,
          MdPaginatorModule

  ],
  exports: [
           // Form  Controls
           MdAutocompleteModule,
           MdCheckboxModule,
           MdDatepickerModule,
           MdInputModule,
           MdRadioModule,
           MdSelectModule,
           MdSliderModule,
           MdSlideToggleModule,
          // Navigation
           MdMenuModule,
           MdSidenavModule,
           MdToolbarModule,
          // Layout
           MdListModule,
           MdGridListModule,
           MdCardModule,
           MdTabsModule,
           // Button & Indicators
           MdButtonModule,
           MdButtonToggleModule,
           MdChipsModule,
           MdIconModule,
           MdProgressSpinnerModule,
           MdProgressBarModule,
           // Popups & Modals
           MdDialogModule,
           MdTooltipModule,
           MdSnackBarModule,
           // Data Table
           MdTableModule,
           MdSortModule,
           MdPaginatorModule

  ],
})
export class MaterialModule { }
