import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule} from '@angular/http';






/*
************************************************
*    Material modules for app
*************************************************
*/
import { LocalStorage } from './localStorage';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
  
  ],
  declarations: [  ],
  providers: [  ],
exports: [  ]
})
export class SharedModule { }
