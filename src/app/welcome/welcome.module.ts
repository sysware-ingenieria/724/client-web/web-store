import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { WelcomeRouting } from './welcome.routing';

import 'hammerjs';

/*
************************************************
*    Material modules for app
*************************************************
*/

import { MaterialModule } from '../app.material';

/*
************************************************
*     principal component
*************************************************
*/
import { AboutComponent } from './about/about.component';
import { CarouselComponent } from './carousel/carousel.component';
import { ContactComponent } from './contact/contact.component';
import { IndexComponent } from './index/index.component';

/*
************************************************
*     modules of  your app
*************************************************
*/
import {  NavigationModule } from "../navigation/navigation.module";
import {  AuthenticationModule } from "../authentication/authentication.module";

/*
************************************************
*     services of  your app
*************************************************
*/

/*
************************************************
*     models of  your app
*************************************************
*/

/*
************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [

    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    RouterModule,
    NavigationModule
    //AuthenticationModule

  ],
  declarations: [AboutComponent, CarouselComponent, ContactComponent, IndexComponent],

  exports:[IndexComponent ]
})
export class WelcomeModule { }
