import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


/********************************
 *          Constantes
 ********************************/
import { LocalStorage } from '../../shared/localStorage';

import { AuthenticationService } from '../authentication.service';

import { Auth } from '../auth';


@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  title: string = 'Iniciar Session';
  isLoggedIn: boolean = false;
  error: string = '';
  form: FormGroup;
  action: string = 'Iniciar Sesion'
  auth: Auth;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder,
    public locStorage: LocalStorage,
    private authService: AuthenticationService) {
    this.auth = new Auth(null, null);
  }

  ngOnInit() {
    //this.authService.logout();
    this.controlsCreate();
    /*     let url = location.protocol +'//' + location.hostname + '/v1/' + 'auth';
        console.log(url);
        this.login(); */

        this.loadData();
  }

  // start controls from from
  controlsCreate() {
    this.form = this.fb.group({
      usuario: ['fernando', Validators.required],
      clave: ['fernando', Validators.required],
    });
  }
  loadData(){
    this.form.value['usuario']="fernando";
    this.form.value['clave']="fernando";
  }

  login() {
    console.log("inicio a hacer login");
    this.locStorage.cleanSession();
    this.auth.usuario = this.form.value['usuario'];
    this.auth.clave = this.form.value['clave'];

    this.authService.login(this.auth)
      .subscribe(
      result => {
        console.log(result)
        if (result === true) {
          this.cargarPerfil();
          console.log(result);
          return;
        } else {
          //this.openDialog();
          return;
        }
      })




  }

  cargarPerfil() {
    alert('Angular Auth');
    this.router.navigate(['/dashboard']);

  }

  volver() {
    let link = ['/'];
    this.router.navigate(link);

  }

}
