import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { HttpModule} from '@angular/http';


import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';

import { AuthenticationService } from './authentication.service';
import { NotAuthGuard } from './not-auth.guard';

import { LocalStorage } from '../shared/localStorage';


/*
************************************************
*    Material modules for app
*************************************************
*/
import { MaterialModule } from '../app.material';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
      BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,

    RouterModule,
    MaterialModule
  ],
  declarations: [LoginComponent, LogoutComponent],
  providers: [ AuthenticationService, NotAuthGuard, LocalStorage ],
exports: [ LoginComponent, LogoutComponent ]
})
export class AuthenticationModule { }
