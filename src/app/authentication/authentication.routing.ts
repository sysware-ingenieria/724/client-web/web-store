import { Routes } from '@angular/router';


import { LoginComponent }  from './login/login.component';
import { LogoutComponent }  from './logout/logout.component';


export const AuthenticationRouting: Routes = [
  { path: 'auth', component: null,
  children: [
      { path: '', redirectTo: 'login', pathMatch: 'full'},
      { path: 'login', component: LoginComponent },
      { path: 'register', component: LogoutComponent },
  ]
},

]
