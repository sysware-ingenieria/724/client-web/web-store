import {Routes} from '@angular/router';


/***************************************************
 * Llamar el Componente que se muestra por defecto *
 ***************************************************/
//import { IndexComponent } from './welcome/index/index.component'

/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { WelcomeRouting }  from './welcome/welcome.routing'
import { AuthenticationRouting }  from './authentication/authentication.routing'
import { DashboardRouting }  from './dashboard/dashboard.routing'



export const AppRouting: Routes = [
  ...WelcomeRouting,
  ...AuthenticationRouting,
  ...DashboardRouting,
  { path: '*', redirectTo:'/',pathMatch:'full'}
];
