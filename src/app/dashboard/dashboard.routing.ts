import { Routes } from '@angular/router';

import { AdminRouting }  from './admin/admin.routing'
import { BusinessRouting }  from './business/business.routing';

import { MainComponent } from './main/main.component';

export const DashboardRouting: Routes= [
    { path: 'dashboard', component: MainComponent,
        children: [
            ...BusinessRouting,
            ...AdminRouting
        ]
    },

];
