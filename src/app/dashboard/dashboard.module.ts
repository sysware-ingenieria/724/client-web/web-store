import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {MdSidenav, MdSidenavModule} from '@angular/material';
/*
*************************************************
*     Principal Routing
*************************************************
*/
import { DashboardRouting } from './dashboard.routing';
/*
*************************************************
*     Principal component
*************************************************
*/

/*
************************************************
*     modules of  your app
*************************************************
*/
import { MaterialModule } from '../app.material';
import { NavigationModule } from '../navigation/navigation.module';
import { FooterModule } from '../navigation/footer/footer'
import {ComponentHeaderModule} from '../navigation/component-page-header/component-page-header';
/*
*************************************************
*     services of  your app
*************************************************
*/
import {ComponentPageTitle} from '../navigation/page-title/page-title';
/*
*************************************************
*     models of  your app
*************************************************
*/
import {  AdminModule } from "../dashboard/admin/admin.module";
import {  BusinessModule } from "../dashboard/business/business.module";
import { MainComponent } from './main/main.component';

/*
*************************************************
*     constant of  your app
*************************************************
*/


@NgModule({
  imports: [
    CommonModule,
    RouterModule,

    MaterialModule,

    NavigationModule,
    FooterModule,
    ComponentHeaderModule,

    AdminModule,
    BusinessModule,


  ],
  providers:[ComponentPageTitle],
  declarations: [MainComponent]
})
export class DashboardModule { }
