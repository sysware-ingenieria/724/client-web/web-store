import { Routes } from '@angular/router';


/***************************************************************
 * Llamar a todas las Rutas que tenga en cada Modulo de la App  *
 ***************************************************************/
import { ClientRouting }  from './client/client.routing'

export const BusinessRouting: Routes = [


  { path: 'business', component: null,
    children: [
        { path: '', redirectTo: 'client', pathMatch: 'full'},
        ...ClientRouting,
    ]
  },


]
