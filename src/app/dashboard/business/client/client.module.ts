import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TypeThirdComponent } from './type-third/type-third.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [TypeThirdComponent],
  exports:[TypeThirdComponent]
})
export class ClientModule { }
