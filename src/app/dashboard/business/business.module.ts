import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { LocalStorage } from '../../shared/localStorage';

/*
************************************************
*     modules of  your app
*************************************************
*/
import {  ClientModule } from "./client/client.module";
@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ClientModule
  ],
  providers:[LocalStorage],
  declarations: []
})
export class BusinessModule { }
