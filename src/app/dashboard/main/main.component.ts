import { Component, ViewEncapsulation, ViewChild, OnInit } from '@angular/core';

import {MdSidenav, MdSidenavModule} from '@angular/material';
import {Router} from '@angular/router';

import { ComponentPageTitle } from '../../navigation/page-title/page-title';

const SMALL_WIDTH_BREAKPOINT = 840;

import { Menu } from '../../shared/menu';
import { Token } from '../../shared/token';

import { LocalStorage } from '../../shared/localStorage';

import { AuthenticationService } from '../../authentication/authentication.service'

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MainComponent implements OnInit {


    menusLista:Menu[];
    token:Token;

      @ViewChild(MdSidenav) sidenav: MdSidenav;

    logoLista=[{id:1,img:'bavaria.png'},
              {id:2,img:'colgate.png'},
              {id:3,img:'cocacola.png'},
              {id:4,img:'lifebuoy.png'},
              {id:5,img:'nescafe.png'},
              {id:6,img:'pepsi.png'},
              {id:7,img:'lays.png'},
              {id:8,img:'knorr.png'},
              {id:9,img:'dove.png'},
              {id:10,img:'tide.jpg'}]

                myLogo:any;

    constructor(public locStorage: LocalStorage,
        public _componentPageTitle: ComponentPageTitle,
      private _router: Router,
                private authService:AuthenticationService) { }

    ngOnInit() {
      /** @todo elimanar la asiignación de token

      */
      this.token=new Token(32312,2323,"Luis",null,null)

      this._router.events.subscribe(() => {
          if (this.isScreenSmall()) {
            this.sidenav.close();
          }
        });


        let session=this.locStorage.getSession();
       if(!session){
         /**
         @todo Eliminar comentario para
         */
           //this.Login();
       }else{
         this.menusLista=this.locStorage.getMenu();
         this.token=this.locStorage.getToken();


       }

         this._componentPageTitle.title = 'Menu Principal';



    }


    isScreenSmall(): boolean {
    return window.matchMedia(`(max-width: ${SMALL_WIDTH_BREAKPOINT}px)`).matches;
  }

  Login() {
      let link = ['/auth'];
      this._router.navigate(link);
    }

    Logout() {
     this.locStorage.cleanSession();
     this.goIndex();

    }

    goIndex() {
      let link = ['/'];
      this._router.navigate(link);
    }

    goDashboard() {
      let link = ['/dashboard'];
      this._router.navigate(link);

    }
    Perfil() {
      alert("EN DESARROLLO");

    }
}
