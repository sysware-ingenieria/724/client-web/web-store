import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
  selector: 'app-nav-toolbar',
  templateUrl: './nav-toolbar.component.html',
  styleUrls: ['./nav-toolbar.component.css']
})
export class NavToolbarComponent implements OnInit {
  title="Gallery";

  user=false;
  constructor(private authService:AuthenticationService,
              private router:Router) { }

  ngOnInit() {
    this.user=false;

  }

  logOut(){
    this.user!=this.user;
  }

}
