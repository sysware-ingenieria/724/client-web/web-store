import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import {MdSidenav, MdSidenavModule} from '@angular/material';

/*
************************************************
*    Material modules for app
*************************************************
*/
//import { MaterialModule } from '@angular/material';
import { MaterialModule } from '../app.material';
import 'hammerjs';
/*
************************************************
*     components
*************************************************
*/

import { NavToolbarComponent } from './nav-toolbar/nav-toolbar.component';

/*
************************************************
*     services of  your app
*************************************************
*/
import { AuthenticationService } from '../authentication/authentication.service';
import { SidenavComponent } from './sidenav/sidenav.component';


@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MdSidenavModule, RouterModule,
    MaterialModule

  ],

  declarations: [ NavToolbarComponent, SidenavComponent],
  providers:[AuthenticationService],
  exports:[NavToolbarComponent,SidenavComponent]
})
export class NavigationModule { }
